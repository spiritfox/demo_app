// 自带的库
require('babel-polyfill');
const proxyServer="https://www.efzxt.com";
const path = require('path');
const webpack = require('webpack');
const devMode = true;
const AddAssetHtmlCdnWebpackPlugin = require('add-asset-html-cdn-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');
const copyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
    mode: "development",//env,
    // devtool: "inline-source-map",
    entry: {
        bundle: ['babel-polyfill', './src/main.js'],
    },
    output: {
        publicPath: "/",
        path: path.resolve(__dirname, '../dist'), // 必须使用绝对地址，输出文件夹
        filename: "[name].js", // 打包后输出文件的文件名
        chunkFilename: "js/[name].js"
    },
    devServer: {
        // open: false,
        // disableHostCheck: true,
        // watchContentBase: true,
        // hotOnly: true,
        // host: bConfig.host,
        // port: 3033, //端口
        contentBase: path.join(__dirname, '../dist'),
        proxy: {
            '/api': {
                target: proxyServer,
                pathRewrite: {'^/api': ''},
                changeOrigin: true,     // target是域名的话，需要这个参数，
                secure: true,          // 设置支持https协议的代理
            }
        }
    },
    externals: {
        jquery: 'jQuery' // '包名':'全局变量'
    },
    plugins: [
        new webpack.ProvidePlugin({
            // $: 'jquery',
            // jQuery: 'jquery'
        }),
        new htmlPlugin({
            minify: {
                collapseWhitespace: false,
                removeComments: false,
                removeRedundantAttributes: false,
                removeScriptTypeAttributes: false,
                removeStyleLinkTypeAttributes: false,
                useShortDoctype: false
            },
            chunks: ['bundle'],//correspond js file
            filename: "index.html",
            hash: true,
            template: './src/main.html'
        }),
        // new htmlPlugin({
        //     minify: {
        //         removeAttributeQuotes: true
        //     },
        //     chunks: ['indirect'],//correspond js file
        //     filename: "indirect.html",
        //     hash: true,
        //     template: './indirect.html'
        // }),
        // new copyWebpackPlugin([
        //     {
        //         from: './static',//打包的静态资源目录地址
        //         to: './static' //打包到dist下面的public
        //     }
        // ]),
        new AddAssetHtmlCdnWebpackPlugin(true, {
            'jquery': 'https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/jquery/jquery-1.11.0.min.js',
            // "summernote-lite_css": "./static/summernote/summernote-lite.css",
            // "summernote-lite_js": "./static/summernote/summernote-lite.js"
            // "template": "https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/template_engine/template-web.js",
            // "base64": "https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/repositories/secure/base64.min.js",
            // "plupload": "static/plupload/plupload.full.min.js"
        })
    ],
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                // include: [path.resolve(__dirname, "../src"), path.resolve(__dirname, "../view")],
                use: {
                    loader: 'babel-loader?cacheDirectory=true',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            ['@babel/plugin-transform-runtime'],
                            ['@babel/plugin-transform-modules-commonjs'],
                            ['@babel/plugin-transform-object-assign']
                        ]
                    }
                }
            },
            {
                test: /\.css$/i,
                exclude: /\.lazy\.css$/i,
                use: ['style-loader', {
                    loader: 'css-loader',
                    options: {
                        sourceMap: devMode //启用css调试路径
                    }
                }]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: 'sass-loader'}
                ]
            },
            {
                test: /\.lazy\.css$/i,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'lazyStyleTag',
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: devMode //启用css调试路径
                        }
                    }
                ],
            },

            {
                test: /\.(png|jpe?g|gif)$/i,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 1000, /* 图片大小小于1000字节限制时会自动转成 base64 码引用*/
                            name: '[name].[ext]',
                            // outputPath: '../dist/images/'
                            publicPath: './',
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|eot|woff|woff2|svg)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    limit: 100000,
                    // publicPath: './dist/fonts/',
                    name: 'fonts/[name].[ext]',
                },
            },
            // 匹配所有Html loaders
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'text-loader',
                }],
            }
        ],
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            "@src": path.resolve(__dirname, "../src")
        }
    }
};
