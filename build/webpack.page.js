//build pages
const path = require('path');
const glob = require("glob");
const htmlPlugin = require('html-webpack-plugin');

const htmlPath = './view/share/*.html';
const jsPath = './view/share/module/*.js';

function getView(globPath, flag) {
    let files = glob.sync(globPath);

    let entries = {},
        entry, dirname, basename, pathname, extname;

    files.forEach(item => {
        entry = item;
        dirname = path.dirname(entry);//当前目录
        extname = path.extname(entry);//后缀
        basename = path.basename(entry, extname);//文件名
        pathname = path.join(dirname, basename);//文件路径
        if (extname === '.html') {
            entries[pathname] = './' + entry;
        } else if (extname === '.js') {
            entries[basename] = ['babel-polyfill',entry];
        }
    });
    return entries;
}

let pages = Object.keys(getView(htmlPath));
let htmlPlugins = [];
pages.forEach(pathname => {
    const separator = pathname.substring(0, pathname.lastIndexOf("/") + 1);
    // console.log(separator);
    let htmlName = pathname.split(separator)[1];
    // console.log(pathname)
    // console.log("==============");
    // console.log(htmlName);
    let conf = {
        filename: `share/${htmlName}.html`,
        template: `${pathname}.html`,
        //hash: true,
        //chunks: ['common',htmlname],
        chunks: [htmlName],
        minify: {
            collapseWhitespace: true,
            removeComments: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            useShortDoctype: true
        }
    };
    htmlPlugins.push(new htmlPlugin(conf));
});
const entriesObj = getView(jsPath);


exports.entriesObj = entriesObj;
exports.htmlPlugins = htmlPlugins;