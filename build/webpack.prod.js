// 自带的库

require('babel-polyfill');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const AddAssetHtmlCdnWebpackPlugin = require('add-asset-html-cdn-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');
const copyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;//分析包的大小
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const optimizeCss = require('optimize-css-assets-webpack-plugin');//css compress
const compiler = webpack({
    // webpack options
});
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    mode: "production",
    optimization: {
        minimizer: [
            //js 压缩
            new UglifyJsPlugin({
                exclude: /node_modules/,
                uglifyOptions: {
                    ie8: true // 解决ie下的关键字default的问题
                }
            }),
            //css compress
            new optimizeCss({})
        ],
        // splitChunks: {
        //     cacheGroups: {
        //         commons: {
        //             name: 'bundle',
        //             chunks: 'initial',
        //             minChunks: 1
        //         }
        //     }
        // }
    },
    entry: {
        bundle: ['babel-polyfill', './src/common/plugin/layui/layui.all.js', './view/main.js'],
        indirect: "./view/indirect.js",
    }, // 入口文件
    output: {
        publicPath: "./",
        path: path.resolve(__dirname, '../dist'), // 必须使用绝对地址，输出文件夹
        filename: "[name].js", // 打包后输出文件的文件名
        chunkFilename: "js/[chunkhash:10].js"
    },
    externals: {
        jquery: 'jQuery', // '包名':'全局变量'
        // summernote_lite:"summernote-lite",
        // "template": "template"
    },
    plugins: [
        new webpack.ProvidePlugin({
            // $: 'jquery',
            // jQuery: 'jquery'
        }),
        new htmlPlugin({
            minify: {
                removeAttributeQuotes: true
            },
            chunks: ['bundle'],//correspond js file
            filename: "index.html",
            hash: true,
            template: './view/main.html'
        }),
        new htmlPlugin({
            minify: {
                removeAttributeQuotes: true
            },
            chunks: ['indirect'],//correspond js file
            filename: "indirect.html",
            hash: true,
            template: './indirect.html'
        }),
        new htmlPlugin({
            minify: {
                removeAttributeQuotes: true
            },
            chunks: ['main'],//correspond js file
            filename: "view/main.html",
            hash: true,
            template: './main.html'
        }),
        new copyWebpackPlugin([
            {
                from: './css/modules',//打包的静态资源目录地址
                to: './css/modules' //打包到dist下面的public
            },
            {
                from: './static',//打包的静态资源目录地址
                to: './static' //打包到dist下面的public
            }
        ]),
        new optimizeCss({
            assetNameRegExp: /\.style\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {discardComments: {removeAll: true}},
            canPrint: true
        }),
        new ExtractTextPlugin('[name].css'),
        new AddAssetHtmlCdnWebpackPlugin(true, {
            'jquery': 'https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/jquery/jquery-1.11.0.min.js',
            "summernote-lite_css":"https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/summernote/summernote-lite.css",
            "summernote-lite_js":"https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/summernote/summernote-lite.js",
            "template": "https://wlypublic.oss-cn-beijing.aliyuncs.com/web_plugins/template_engine/template-web.js",
            "plupload": "static/plupload/plupload.full.min.js"
        })
    ],
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            ['@babel/plugin-transform-runtime'],
                            ['@babel/plugin-transform-modules-commonjs'],
                            ['@babel/plugin-transform-object-assign']
                        ]
                    }
                }
            },
            {
                test: /\.css$/i,
                exclude: /\.lazy\.css$/i,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                        }
                    ]
                })
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: 'sass-loader'}
                ]
            },
            {
                test: /\.lazy\.css$/i,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'lazyStyleTag',
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {}
                    }
                ],
            },

            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 1000, /* 图片大小小于1000字节限制时会自动转成 base64 码引用*/
                            name: '[name].[ext]',
                            // outputPath: '../dist/images/'
                            publicPath: './',
                            //publicPath: './',
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|eot|woff|woff2|svg)$/,
                loader: 'file-loader',
                options: {
                    limit: 100000,
                    // publicPath: './dist/fonts/',
                    name: 'fonts/[name].[ext]',
                },
            },
            // 匹配所有Html loaders
            {
                test: /\.html$/,
                use: [{
                    loader: 'text-loader',
                }],
                // test: /\.html$/,
                // use: [
                //     {//对html动态取名
                //         loader: 'text-loader',
                //         options: {
                //             name: "[name][hash].html"
                //         }
                //     },
                //     {//让html和bundle.js进行区分,因为之前加载css,js的时候都会融入到出口的bundle里面去,
                //         loader: "extract-loader"
                //     },
                //     {/*
                //         它会帮你找到html的文件然后会借助你的extract-loader跟你的js进行分离
                //         然后file-loader会根据你html然后取名
                //         */
                //         loader: "text-loader"
                //     }
                // ]
            }
        ],
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            "THeader": path.resolve(__dirname, "../view/components/common/THeader"),
            "com": path.resolve(__dirname, "../src/common/util/common"),
            "mui": path.resolve(__dirname, "../src/common/plugin/mui.min"),
            "Modal": path.resolve(__dirname, "../view/components/common/Modal"),
            "MediaElement": path.resolve(__dirname, "../src/common/plugin/video/mediaelement/MediaElement"),
            "MediaElementPlayer": path.resolve(__dirname, "../src/common/plugin/video/mediaelement/build/mediaelement-and-player"),
            "MyUpload": path.resolve(__dirname, "../src/common/plugin/MyUpload"),
            "MyPlupload": path.resolve(__dirname, "../src/common/components/upload/MyPlupload"),
            "ResourceService": path.resolve(__dirname, "../src/common/services/ResourceService"),
            "ClipboardJS": path.resolve(__dirname, "../src/common/plugin/clipboard/clipboard"),
            "ShareTool": path.resolve(__dirname, "../src/common/plugin/ShareTool"),
            "UtilityHelper": path.resolve(__dirname, "../src/common/helper/UtilityHelper"),
            "ZmCanvasCrop": path.resolve(__dirname, "../src/common/components/upload/ZmCanvasCrop"),
            "openApplication": path.resolve(__dirname, "../src/common/plugin/openApplication"),
            "widget_1": path.resolve(__dirname, "../src/common/components/widget_1"),
            "NavBar": path.resolve(__dirname, "../src/common/util/NavBar"),
            "ViewHelper": path.resolve(__dirname, "../src/common/helper/ViewHelper"),
            "label_select": path.resolve(__dirname, "../src/common/components/label_select"),
            "resource_filter": path.resolve(__dirname, "../src/common/components/resource_filter"),
            "resource_label": path.resolve(__dirname, "../src/common/components/resource_label"),
            "right_widget": path.resolve(__dirname, "../src/common/components/right_widget"),
            "touchTouch": path.resolve(__dirname, "../src/common/plugin/image_preview/js/touchTouch.jquery"),

            "vue": "vue/dist/vue.js",
            "@css": path.resolve(__dirname, "../css"),
            "@src": path.resolve(__dirname, "../src"),
            "@view": path.resolve(__dirname, "../view"),
            "@quit": path.resolve(__dirname, "../view/components/quit"),
            "@live": path.resolve(__dirname, "../view/components/live"),
            "@video": path.resolve(__dirname, "../view/components/video"),
            "@school": path.resolve(__dirname, "../view/components/school"),
            "@doc": path.resolve(__dirname, "../view/components/doc"),
            "@me": path.resolve(__dirname, "../view/components/me"),
            "@student/home": path.resolve(__dirname, "../view/student/components/home"),
            "@student/live": path.resolve(__dirname, "../view/student/components/live"),
            "@student/video": path.resolve(__dirname, "../view/student/components/video"),
            "@student/doc": path.resolve(__dirname, "../view/student/components/doc"),
            "@student/me": path.resolve(__dirname, "../view/student/components/me"),
        }
    }
};
