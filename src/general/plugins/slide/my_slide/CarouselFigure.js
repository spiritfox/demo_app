export default class CarouselFigure {
    constructor(args) {
        return (async () => {
            this.inject = args.inject;
            this.callbackFn = args.callbackFn;
            this.currentIndex = 0;
            this.totalQuantity = 3;
            this.dragEnable = false;
            this.touchOriginX = 0;
            this.touchOriginY = 0;
            this.touchMoveX = 0;
            this.touchMoveY = 0;
            this.currentElement = null;
            this.activating = false;
            this.animateTime = 200;
            this.timer = null;

            await this.initView(this.inject);
            this.scrollX(0);
            // startUpTimer();
            this.dragEvent();
        }).call(this);
    }

    async initView() {
        const _html = await import("@src/general/plugins/slide/my_slide/template/carousel_figure.html");
        $(this.inject).empty().append(_html["default"]);
    }

    previousImage() {
        --this.currentIndex;
        if (this.currentIndex < 0) {
            this.currentIndex = 0;
        }
        return this.currentIndex;
    }

    nextImage(loop = false) {
        ++this.currentIndex;
        if (this.currentIndex > this.totalQuantity - 1) {
            if (loop) this.currentIndex = 0;
            else this.currentIndex = this.totalQuantity - 1;
        }
        return this.currentIndex;
    }

    scrollX(n) {
        let ele = $('.tab-list').eq(n);           // 当前操作元素
        this.callbackFn({});
        let e_width = ele.outerWidth(),             // 元素占位宽度
            ul = $('.tabs'),                        // 父元素
            w_width = ul.outerWidth(),              // 父元素宽度，即滚动的框的宽度
            scroll_width = ul.scrollLeft();     // 滚动条卷去宽度
        let _x = ele.position().left;              // 相对父元素偏移量，需给父元素添加定位 position
        // 尾部隐藏时，需滚动距离 = 当前操作元素在父元素中偏移量 + 元素占位宽度 - 父元素宽度 + 滚动条卷去宽度
        let offset_left = _x + e_width - w_width + scroll_width;
        if (_x > w_width - e_width) {
            // 尾部被遮挡
            ul.animate({scrollLeft: offset_left}, 200)
        } else if (_x < 0) {
            // 头部被遮挡时，比较简单，直接控制滚动条位置为 :
            // 滚动条当前位置 - 操作元素在父元素中偏移量(此时为负)
            ul.animate({scrollLeft: scroll_width + _x}, 200)
        }
        // this.setElementActive(ele);
    }

    setElementActive(ele) {
        // ele.siblings().removeClass("active main_transparent_bg main_border_color").children("button").removeClass("main_bg");
        // ele.addClass('active main_transparent_bg main_border_color').children("button").addClass("main_bg");
    }

    touchStart(ev, isTouch) {
        // ev.stopPropagation();
        this.clearTimer();
        this.dragEnable = true;
        //获取鼠标按下的坐标
        if (isTouch) {
            this.touchOriginX = ev.originalEvent.targetTouches[0].pageX;
            this.touchOriginY = ev.originalEvent.targetTouches[0].pageY;
        } else {
            this.touchOriginX = ev.clientX;
            this.touchOriginY = ev.clientY;
        }
        //获取元素的left，top值
        // l = _this.currentElement.offsetLeft;
        // t = _this.currentElement.offsetTop;
    }

    touchMove(ev, isTouch) {
        if (!this.dragEnable) return;
        //获取鼠标移动时的坐标
        let x2, y2;
        if (isTouch) {
            x2 = ev.originalEvent.targetTouches[0].pageX;
            y2 = ev.originalEvent.targetTouches[0].pageY;
        } else {
            x2 = ev.clientX;
            y2 = ev.clientY;
        }

        //计算出鼠标的移动距离
        this.touchMoveX = x2 - this.touchOriginX;
        this.touchMoveY = y2 - this.touchOriginY;
        //移动的数值与元素的left，top相加，得出元素的移动的距离
        // lt = this.touchMoveY + t;
        // ls = this.touchMoveX + l;
        //更改元素的left，top值
        if (this.currentElement === null) {
            return;
        }
        // currentElement.style.top = lt + 'px';
        // currentElement.style.left = ls + 'px';
        this.currentElement.style.transform = `translateX(${this.touchMoveX}px)`;
    }

    touchEnd(ev,isTouch) {
        this.dragEnable = false;
        this.clearTimer();
        this.startUpTimer();
        if (this.touchMoveX > 30) {
            this.previousImage();
            this.currentElement.style.transform = `translateX(${0}px)`;
            this.scrollX(this.currentIndex);
        }
        if (this.touchMoveX < -30) {
            this.nextImage();
            this.currentElement.style.transform = `translateX(${0}px)`;
            this.scrollX(this.currentIndex);
        }
        if (this.touchMoveX > -30 && this.touchMoveX < 30) {
            this.currentElement.style.transform = `translateX(${0}px)`;
            this.scrollX(this.currentIndex);
        }
    }

    dragEvent() {
        const _this = this;
        //获取需要拖拽的元素
        let l, t, ls, lt, x, y;
        let currentElement = null;

        $(".tab-list img").off().mousedown(function (ev) {
            _this.currentElement = this;
            _this.touchStart(ev);
        }).on('touchstart', function (ev) {
            _this.currentElement = this;
            _this.touchStart(ev, true);
        });
        $(document).mousemove(function (ev) {
            _this.touchMove(ev);
        }).on("touchmove", function (ev) {
            _this.touchMove(ev, true);
        }).mouseup(function (ev) {
            _this.touchEnd(ev);
        }).on("touchend", function (ev) {
            _this.touchEnd(ev,true);
        })
    }

    startUpTimer() {
        const _this = this;
        this.timer = setInterval(function () {
            _this.scrollX(_this.nextImage(true));
        }, 3000);
    }

    clearTimer() {
        clearInterval(this.timer);
    }
}