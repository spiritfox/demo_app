const props = {
    currentIndex: 0,
    totalQuantity: 3,
    callbackFn: null,
    dragEnable: false,
    activating: false,
};

async function initView(inject) {
    const _html = await import("@src/general/plugins/slide/my_slide/template/choose_teacher_slide.html");
    $(inject).empty().append(_html["default"]);
}

export async function injectChooseTeacherSlide(args = {}) {
    await initView(args["inject"]);
    props.callbackFn = args.callbackFn;
    props.totalQuantity = 3;
    scrollX(0);
    dragEvent();
    // bindEvent();
}

function bindEvent() {
    $("#head_btn").off().click(function () {
        props.currentIndex = 0;
        scrollX(0);
    });
    $("#previous_btn").off().click(function () {
        --props.currentIndex;
        if (props.currentIndex < 0) {
            props.currentIndex = 0;
            return;
        }
        scrollX(props.currentIndex);
    });
    $("#next_btn").off().click(function () {
        ++props.currentIndex;
        if (props.currentIndex > props.totalQuantity - 1) {
            props.currentIndex = props.totalQuantity - 1;
            return;
        }
        scrollX(props.currentIndex);
    });
    $("#last_btn").off().click(function () {
        props.currentIndex = props.totalQuantity - 1;
        scrollX(props.currentIndex);
    });
    $(".tab-list").off().click(function () {
        props.currentIndex = parseInt($(this).attr("index"));
        scrollX(props.currentIndex);
    });
}


function scrollX(n) {
    let ele = $('.tab-list').eq(n);           // 当前操作元素
    console.log(ele);
    props.callbackFn({});
    let e_width = ele.outerWidth(),             // 元素占位宽度
        ul = $('.tabs'),                        // 父元素
        w_width = ul.outerWidth(),              // 父元素宽度，即滚动的框的宽度
        scroll_width = ul.scrollLeft();     // 滚动条卷去宽度
    let _x = ele.position().left;              // 相对父元素偏移量，需给父元素添加定位 position
    // 尾部隐藏时，需滚动距离 = 当前操作元素在父元素中偏移量 + 元素占位宽度 - 父元素宽度 + 滚动条卷去宽度
    let offset_left = _x + e_width - w_width + scroll_width;
    if (_x > w_width - e_width) {
        // 尾部被遮挡
        ul.animate({scrollLeft: offset_left}, 200)
    } else if (_x < 0) {
        // 头部被遮挡时，比较简单，直接控制滚动条位置为 :
        // 滚动条当前位置 - 操作元素在父元素中偏移量(此时为负)
        ul.animate({scrollLeft: scroll_width + _x}, 200)
    }
    setElementActive(ele);
}

function setElementActive(ele) {
    ele.siblings().removeClass("active main_transparent_bg main_border_color").children("button").removeClass("main_bg");
    ele.addClass('active main_transparent_bg main_border_color').children("button").addClass("main_bg");
}

function dragEvent() {
    //获取需要拖拽的元素
    let x1, y1, l, t, ls, lt, x, y;
    let currentElement = null;
    $(".tab-list div img").off().mousedown(function (ev) {
        ev.stopPropagation();
        props.dragEnable = true;
        currentElement = this;
        //获取鼠标按下的坐标
        x1 = ev.clientX;
        y1 = ev.clientY;
        //获取元素的left，top值
        l = currentElement.offsetLeft;
        t = currentElement.offsetTop;
    });
    $(document).mousemove(function (ev) {
        ev.stopPropagation();
        if (!props.dragEnable) return;
        //获取鼠标移动时的坐标
        var x2 = ev.clientX;
        var y2 = ev.clientY;

        //计算出鼠标的移动距离
        x = x2 - x1;
        y = y2 - y1;

        //移动的数值与元素的left，top相加，得出元素的移动的距离
        lt = y + t;
        ls = x + l;
        // console.log(lt);
        console.log(x);
        //更改元素的left，top值
        if (currentElement === null) {
            return;
        }
        // currentElement.style.top = lt + 'px';
        // currentElement.style.left = ls + 'px';
        currentElement.style.transform = `translateX(${x}px)`;
    }).mouseup(function (e) {
        props.dragEnable = false;
        if (x > 30) {
            --props.currentIndex;
            scrollX(props.currentIndex);
        }
        if (x < -30) {
            ++props.currentIndex;
            scrollX(props.currentIndex);
        }
    })


    // var divs = document.querySelector('.tab-list');
    //
    // //元素的鼠标落下事件
    // divs.onmousedown = function (ev) {
    //     //event的兼容性
    //     var ev = ev || event;
    //     //获取鼠标按下的坐标
    //     var x1 = ev.clientX;
    //     var y1 = ev.clientY;
    //     //获取元素的left，top值
    //     var l = divs.offsetLeft;
    //     var t = divs.offsetTop;
    //     //给可视区域添加鼠标的移动事件
    //     document.onmousemove = function (ev) {
    //         //event的兼容性
    //         var ev = ev || event;
    //         ev.stopPropagation()
    //
    //         //获取鼠标移动时的坐标
    //         var x2 = ev.clientX;
    //         var y2 = ev.clientY;
    //
    //         //计算出鼠标的移动距离
    //         var x = x2 - x1;
    //         var y = y2 - y1;
    //
    //         //移动的数值与元素的left，top相加，得出元素的移动的距离
    //         var lt = y + t;
    //         var ls = x + l;
    //         // console.log(lt);
    //         console.log(ls);
    //         if (ls > 30) {
    //             scrollX(0)
    //         }
    //         if (ls < -30) {
    //             scrollX(1)
    //         }
    //
    //         //更改元素的left，top值
    //         divs.style.top = lt + 'px';
    //         divs.style.left = ls + 'px';
    //         divs.style.transform = `translateX(${ls}px)`;
    //
    //     }
    //
    //     //清除
    //     document.onmouseup = function (ev) {
    //
    //         document.onmousemove = null;
    //
    //     }
    //
    // }
}
