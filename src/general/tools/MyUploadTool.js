/**
 * 上传工具
 */
(function () {
    var self = null;
    var instance = null;
    var queue = new Queue();

    var props = {
        host: "",
        address: "",
        params: {} //参数
    };

    function myUpload(options) {
        if (instance == null || !options.singleton) {
            instance = new myobj(options);
            console.log("new instance");
            return instance;
        } else {
            //return instance;
            return new myobj(options);
        }
    }

    function myobj(options) {
        self = this;
        this.auto = options.auto; //自动上传
        this.singleton = options.singleton; //单例模式
        this.scope = options.scope || "public"; //作用域
        //this.shade = options.shade; //遮罩层;
        this.limit = options.limit; //限制
        this._file = options._file;
        this.bucket = options.bucket;
        this._files = options._files;
        this.uploadList = [];
        this.quantity = 1;
        this.lastQuantity = 0;
        this.fileProperty = options.fileProperty || "Blob";
        this.completeFun = options.completeFun; //完成方法
        this.operationCancel = false;
        this.xhr = null; //上传请求
        if (this.auto) {
            if (this._files.constructor.name === "File") {
                queue.enqueue(this._files);
            } else {
                if (this._files.length <= 0) {
                    return;
                }
                for (var i = 0; i < this._files.length; i++) {
                    queue.enqueue(this._files[i]);
                }
            }
            self.loadShade(function () {
                startUpload();
            });
        }
        this.initEvent();
    }

    var proto = myobj.prototype;

    function startUpload() {
        getUploadBucket(self.bucket).then(function (data) {
            props.host = data.host;
            props.address = data.address;
            return before();
        }).then(function () {
            if (self.fileProperty === "Base64") {
                return uploadBase64(queue.front());
            } else {
                return upload(queue.front());

            }
        }).then(function (arg) {
            queue.dequeue();
            self.uploadList.push(arg);
            if (queue.isEmpty()) {
                //上传成功
                if (document.querySelector(".upload_shade")) {
                    document.querySelector(".upload_shade").style.display = "none";
                }
                self.completeFun.call(this, self.uploadList);
                return;
            }
            startUpload();
        }, function (err) {
            console.log(err);
        });
    }

    proto.initEvent = function () {
        window.addEventListener('online', function () {
            //startUpload();
            mui.toast("网络已恢复");
        });
        window.addEventListener('offline', function () {
            mui.toast("网络已断开", 5000);
            self.uploadEnd();
        });
        //浏览器后退事件
        window.addEventListener("popstate", function (e) {
            self.operationCancel = true;
            self.uploadEnd();
            //history.go(1)
        }, false);
    };

    /**
     * 加载遮罩
     */
    proto.loadShade = function (callbackFn) {

    };

    function getUploadBucket(fileType) {
        return new Promise(function (resolve, reject) {
            var p = {
                fileType: fileType,
                platform: "baby"
            };

            $.ajax({
                // headers: NETWORK_CONFIG.headers,
                url: "/UploadAllServlet",
                type: "POST",
                dataType: 'text',
                timeout: 15000,
                data: p,
                success: function (data, status) {
                    resolve(data.data);
                }
            });
        })
    }

    function before() {
        return new Promise(function (resolve, reject) {
            //document.querySelector(".cancelUpload").innerText = "取消";
            var serverUrl = "/UploadAllServlet?i=" + Math.random();
            com.util.request(serverUrl, {}, function (data) {
                var param = {
                    key: "123.png",
                    policy: data.policy,
                    OSSAccessKeyId: data.accessid,
                    success_action_status: "200",
                    signature: data.signature
                };
                props.params = param;
                resolve(param);
            }, function () {
                reject();
                //self.shade.style.display = "none";
            })
        })
    }

    function upload(_file) {
        return new Promise(function (resolve, reject) {
            if (self.lastQuantity.length > self.limit) {
                var obj = {
                    status: "error",
                    message: "只可以上传" + self.limit + "个文件"
                };
                //self.shade.style.display = "none";
                //self.completeFun(obj);
                return;
            }
            var suffix = "." + _file.name.substring(_file.name.lastIndexOf(".") + 1);
            //fetch js  网络请求
            var filename = self.getTimestamp() + suffix;
            props.params.key = props.address + filename;
            var formData = new FormData();
            for (const key in props.params) {
                var paramValue = props.params[key];
                formData.append(key, paramValue);
            }

            var xhr = new XMLHttpRequest();
            self.xhr = xhr;
            xhr.onreadystatechange = function (e) {
                console.log("xhr.readyState=" + xhr.readyState + "  status=" + this.status);
                if (xhr.readyState === 4 && this.status === 200) { //上传成功
                    document.querySelector("[type='file']").value = "";
                    //self.shade.style.display = "none";
                    console.log("上传成功");
                    var obj = {
                        code: "success",
                        filename: _file.name,
                        message: props.host + "/" + props.params.key
                    };
                    resolve(obj);
                    console.log(JSON.stringify(obj));
                    // 图片预览
                } else if (xhr.readyState === 4 && this.status === 0) {
                    if (self.operationCancel) {
                        self.operationCancel = false;
                    } else {
                        mui.toast("如果您第一次看到该提示，尝试再次上传，否则推荐您下载UC浏览器、QQ浏览器、360浏览器", 5000);
                    }
                    self.uploadEnd();
                } else {
                    //reject();
                    //self.shade.style.display = "none";
                    //mui.toast("上传失败");
                }
            };
            //进度监听
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                    self.loadShade(function () {
                        $(".progress_tips").text("已上传：" + percentComplete.toString() + "%");
                        $(".progress_bar").css("width", percentComplete.toString() + "%");
                    });
                } else {
                    console.log("unable to compute");
                }
            }, false);
            xhr.open('POST', props.host);
            //xhr.withCredentials = true;
            //xhr.setRequestHeader("Access-Control-Allow-Origin", "http://localhost:63342");
            //xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            formData.append("file", _file);
            xhr.send(formData);
        })
    }

    function uploadBase64(data) {
        return new Promise(function (resolve, reject) {
            if (self.lastQuantity.length > self.limit) {
                var obj = {
                    status: "error",
                    message: "只可以上传" + self.limit + "个文件"
                };
                //self.shade.style.display = "none";
                //self.completeFun(obj);
                return;
            }
            data = data.split(',')[1];
            data = window.atob(data);
            var ia = new Uint8Array(data.length);
            for (var i = 0; i < data.length; i++) {
                ia[i] = data.charCodeAt(i);
            }
            // canvas.toDataURL 返回的默认格式就是 image/png
            var blob = new Blob([ia], {type: "image/png"});

            var suffix = "." + "png";
            //fetch js  网络请求
            var filename = self.getTimestamp() + suffix;
            props.params.key = props.address + filename;
            var formData = new FormData();
            for (const key in props.params) {
                var paramValue = props.params[key];
                formData.append(key, paramValue);
            }

            var xhr = new XMLHttpRequest();
            self.xhr = xhr;
            xhr.onreadystatechange = function (e) {
                console.log("xhr.readyState=" + xhr.readyState + "  status=" + this.status);
                if (xhr.readyState === 4 && this.status === 200) { //上传成功
                    document.querySelector("[type='file']").value = "";
                    //self.shade.style.display = "none";
                    console.log("上传成功");
                    var obj = {
                        code: "success",
                        filename: filename,
                        message: props.host + "/" + props.params.key
                    };
                    resolve(obj);
                    console.log(JSON.stringify(obj));
                    // 图片预览
                } else if (xhr.readyState === 4 && this.status === 0) {
                    if (self.operationCancel) {
                        self.operationCancel = false;
                    } else {
                        mui.toast("如果您第一次看到该提示，尝试再次上传，否则推荐您下载UC浏览器、QQ浏览器、360浏览器", 5000);
                    }
                    self.uploadEnd();
                } else {
                    //reject();
                    //self.shade.style.display = "none";
                    //mui.toast("上传失败");
                }
            };
            //进度监听
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                    self.loadShade(function () {
                        $(".progress_tips").text("已上传：" + percentComplete.toString() + "%");
                        $(".progress_bar").css("width", percentComplete.toString() + "%");
                    });
                } else {
                    console.log("unable to compute");
                }
            }, false);
            xhr.open('POST', props.host);
            //xhr.withCredentials = true;
            //xhr.setRequestHeader("Access-Control-Allow-Origin", "http://localhost:63342");
            //xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            formData.append("file", blob);
            xhr.send(formData);
        })
    }

    /**
     * 上传结束
     */
    proto.uploadEnd = function () {
        if (self.xhr) {
            self.xhr.abort();
        }
        queue.clear();
        $(".progress_bar").css("width", "0%");
        document.querySelector(".upload_shade").style.display = "none";
        $("#pickupFile").val("");
    };

    proto.getDate = function (htm) {
        var myDate = new Date();
        var year = myDate.getFullYear();
        var month = Number(myDate.getMonth()) + 1;
        //month = month < 10 ? ("0" + month) : month;
        return year + "-" + month;
    };

    proto.getTimestamp = function () {
        return new Date().getTime();
    };
    this.MyUpload = myUpload;
}(this));

function Queue() {
    this.items = [];
    this.enqueue = function (element) {
        if (element.constructor.name === "Array") {
            Array.prototype.push.apply(this.items, element);
            return;
        }
        this.items.push(element);
    };
    this.dequeue = function () {
        return this.items.shift();
    };
    this.front = function () {
        return this.items[0];
    };
    this.back = function () {
        if (this.isEmpty()) return 'This queue is empty';
        else return this.items[this.items.length - 1];
    };
    this.isEmpty = function () {
        return this.items.length === 0;
    };
    this.clear = function () {
        this.items = [];
    };
    this.size = function () {
        return this.items.length;
    };
    this.print = function () {
        console.log(this.items.toString());
    };
}



